<?php

class Users 
{

    public ?string $nom;

    public function __construct(
        string $nom = null
    )
    {
        $this->nom = $nom;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
    
}