<?php

require_once "User.php";

class Subscriber extends Users
{
    public ?int $id;
    public ?string $mdp;

    public function __construct
    (

        ?int $id = null,
        ?string $mdp = null
    )
    {   

        $this->id = $id;
        parent::__construct();
        $this->mdp = $mdp;
    }

    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp(): string
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp(string $mdp)
    {
        $this->mdp = $mdp;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public static function ClassFromArray(array $tab): ?Subscriber
    {
        $subscriber = new static();
        foreach ($tab as $key => $value) {
            $subscriber->$key = $value;
        }
        return $subscriber;
    }
}