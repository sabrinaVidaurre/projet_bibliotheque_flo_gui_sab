<?php

require_once "Subscriber.php";
require_once "Book.php";

class Reservation
{
    
    private String $date;  
    private Subscriber $subscriberId;
    private Book $bookId;

    public function __construct
    (
        ?String $date,
        ?Subscriber $subscriberId,
        ?Book $bookId
    )
    {
        $this->date = $date;
        $this->subscriberId = $subscriberId;
        $this->bookId = $bookId;
    }

    /**
     * Get the value of date
     */ 
    public function getDate(): String
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate(String $date)
    {
        $this->date = $date;
    }

    /**
     * Get the value of subscribersId
     */ 
    public function getSubscriberId(): Subscriber
    {
        return $this->subscriberId;
    }

    /**
     * Set the value of subscribersId
     *
     * @return  self
     */ 
    public function setSubscriberId(Subscriber $subscriberId)
    {
        $this->subscriberId = $subscriberId;
    }

    /**
     * Get the value of booksId
     */ 
    public function getBookId(): Book
    {
        return $this->bookId;
    }

    /**
     * Set the value of booksId
     *
     * @return  self
     */ 
    public function setBookId(Book $bookId)
    {
        $this->bookId = $bookId;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}