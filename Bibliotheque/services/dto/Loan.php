<?php

require_once "Subscriber.php";
require_once "Book.php";

class Loan
{
    private ?int $id;
    private ?DateTime $dateDebut;  
    private ?DateTime $dateFin;  
    private ?DateTime $dateRetour;  
    private ?Subscriber $subscriberId;
    private ?Book $bookId;

    public function __construct
    (
        ?int $id = null,
        ?DateTime $dateDebut = null,
        ?DateTime $dateFin = null,
        ?DateTime $dateRetour = null,
        ?Subscriber $subscriberId = null,
        ?Book $bookId = null
    )
    {
        $this->id = $id;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
        $this->dateRetour = $dateRetour;
        $this->subscriberId = $subscriberId;
        $this->bookId = $bookId;
    }

    
    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of dateDebut
     */ 
    public function getDateDebut(): DateTime
    {
        return $this->dateDebut;
    }

    /**
     * Set the value of dateDebut
     *
     * @return  self
     */ 
    public function setDateDebut(DateTime $dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * Get the value of dateFin
     */ 
    public function getDateFin(): DateTime
    {
        return $this->dateFin;
    }

    /**
     * Set the value of dateFin
     *
     * @return  self
     */ 
    public function setDateFin(DateTime $dateFin)
    {
        $this->dateFin = $dateFin;

    }

    /**
     * Get the value of dateRetour
     */ 
    public function getDateRetour(): DateTime
    {
        return $this->dateRetour;
    }

    /**
     * Set the value of dateRetour
     *
     * @return  self
     */ 
    public function setDateRetour(DateTime $dateRetour)
    {
        $this->dateRetour = $dateRetour;
    }

    /**
     * Get the value of subscribersId
     */ 
    public function getSubscriberId(): Subscriber
    {
        return $this->subscriberId;
    }

    /**
     * Set the value of subscribersId
     *
     * @return  self
     */ 
    public function setSubscriberId(Subscriber $subscriberId)
    {
        $this->subscriberId = $subscriberId;
    }

    /**
     * Get the value of booksId
     */ 
    public function getBookId(): Book
    {
        return $this->bookId;
    }

    /**
     * Set the value of booksId
     *
     * @return  self
     */ 
    public function setBookId(Book $bookId)
    {
        $this->bookId = $bookId;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public static function ClassFromArray(array $tab): Loan
    {
        $loan = new self();
        foreach ($tab as $key => $value) {
            $loan->$key = $value;
        }
        return $loan;
    }
}

