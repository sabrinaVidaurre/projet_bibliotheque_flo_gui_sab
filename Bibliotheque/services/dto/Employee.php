<?php

class Employee
{

    public ?int $id;

    public function __construct
    (
        ?int $id = null
    )
    {
        $this->id = $id;
    }

    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    public static function ClassFromArray(array $tab): Employee
    {
        $employee = new static();
        foreach ($tab as $key => $value) {
            $employee->$key = $value;
        }
        return $employee;
    }
}