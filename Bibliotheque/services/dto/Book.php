<?php 

class Book
{

    private ?int $id;
    private ?string $titre;
    private ?int $nbLivre;

    public function __construct
    (
        ?int $id = null,
        ?string $titre = null,
        ?int $nbLivre = null

    )
    {
        $this->id = $id;
        $this->titre = $titre;
        $this->nbLivre = $nbLivre;
    }

    /**
     * Get the value of id
     */ 
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of titre
     */ 
    public function getTitre(): String
    {
        return $this->titre;
    }

    /**
     * Set the value of titre
     *
     * @return  self
     */ 
    public function setTitre(string $titre)
    {
        $this->titre = $titre;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * Get the value of nbLivre
     */ 
    public function getNbLivre(): int
    {
        return $this->nbLivre;
    }

    /**
     * Set the value of nbLivre
     *
     * @return  self
     */ 
    public function setNbLivre(int $nbLivre)
    {
        $this->nbLivre = $nbLivre;
    }

    public static function ClassFromArray(array $tab): Book
    {
        $book = new static();
        foreach ($tab as $key => $value) {
            $book->$key = $value;
        }
        return $book;
    }
}