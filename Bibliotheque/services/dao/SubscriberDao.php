<?php

require_once "ConstanteDao.php";
require_once "C:\wamp64\www\Bibliotheque\services\dto\Subscriber.php";

class SubscriberDao
{
    private const FILE_SAVE_SUBSCRIBER = "C:\wamp64\www\Bibliotheque\data\save_subscriber.csv";
    private const FILE_CPT_SUBSCRIBER = "C:\wamp64\www\Bibliotheque\data\cpt_subscriber.txt";
    private const CHAMP_ID = "id";
    private const CHAMP_NOM = "nom";
    private const CHAMP_MDP = "mdp";
    private const ENTETES_SUB = 
    [SubscriberDao::CHAMP_ID,
    SubscriberDao::CHAMP_NOM,
    SubscriberDao::CHAMP_MDP];

    public function save(Subscriber $newSub): Subscriber
    {
        $handle = fopen(SubscriberDao::FILE_SAVE_SUBSCRIBER, ConstanteDao::FILE_OPTION_A_PLUS);
        $newSub->setId(str_pad($this->getNextId(), 3, "0", STR_PAD_LEFT));
        fputcsv($handle, $newSub->toArray(), ConstanteDao::DELIM);
        fclose($handle);
        return $newSub;
    }

    public function getNextId(): int
    {
        $handle = fopen(SubscriberDao::FILE_CPT_SUBSCRIBER, ConstanteDao::FILE_OPTION_A_PLUS);
        $currentId = intval(fgets($handle));
        fclose($handle);
        $handle = fopen(SubscriberDao::FILE_CPT_SUBSCRIBER, ConstanteDao::FILE_OPTION_W_PLUS);
        fputs($handle, $currentId+1);
        fclose($handle);
        return $currentId;
    }
    public function getById($motif): Subscriber
    {
        return $this->getOneByAttribute(SubscriberDao::CHAMP_ID, $motif);
    }

    public function getAll(): array
    {
        $handle = fopen(SubscriberDao::FILE_CPT_SUBSCRIBER, ConstanteDao::FILE_OPTION_R);
        $entities = [];

        $entetes = fgetcsv($handle, 0, ConstanteDao::DELIM);

        while (($entity = fgetcsv($handle, 0, ConstanteDao::DELIM)) != false) {
            $entities[] = Subscriber::ClassFromArray(array_combine($entetes, $entity));
        }

        fclose($handle);
        return $entities;
    }

    public function getOneByAttribute(string $attribute, string $motif): Subscriber
    {
        $allEntities = $this->getAll();
        foreach ($allEntities as $entity) {
            $getter = "get".ucfirst($attribute);
            if (strtolower($entity->$getter()) === strtolower($motif)) {
                return $entity;
            }
        }
        return null;
    }
}