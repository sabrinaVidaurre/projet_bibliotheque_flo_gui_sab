<?php

require_once "ConstanteDao.php";
require_once "C:\wamp64\www\Bibliotheque\services\dto\Loan.php";

class LoanDao
{
    private const FILE_SAVE_LOAN = "C:\wamp64\www\Bibliotheque\data\save_loan.csv";
    private const FILE_CPT_LOAN = "C:\wamp64\www\Bibliotheque\data\cpt_loan.txt";
    private const CHAMP_ID = "id";
    private const CHAMP_DATE_DEBUT = "dateDebut";
    private const CHAMP_DATE_FIN = "dateFin";
    private const CHAMP_DATE_RETOUR = "dateRetour";
    private const CHAMP_ID_BOOK = "bookId";
    private const CHAMP_ID_SUB = "subscriberId";
    private const ENTETES_BOOKS = 
    [LoanDao::CHAMP_ID,
    LoanDao::CHAMP_DATE_DEBUT,
    LoanDao::CHAMP_DATE_FIN,
    LoanDao::CHAMP_DATE_RETOUR,
    LoanDao::CHAMP_ID_BOOK,
    LoanDao::CHAMP_ID_SUB];

    public function save(Loan $newLoan): Loan
    {
        $handle = fopen(LoanDao::FILE_SAVE_LOAN, ConstanteDao::FILE_OPTION_A_PLUS);
        $newLoan->setId(str_pad($this->getNextId(), 3, "0", STR_PAD_LEFT));
        fputcsv($handle, $newLoan->toArray(), ConstanteDao::DELIM);
        fclose($handle);
        return $newLoan;
    }

    public function getNextId(): int
    {
        $handle = fopen(LoanDao::FILE_CPT_LOAN, ConstanteDao::FILE_OPTION_A_PLUS);
        $currentId = intval(fgets($handle));
        fclose($handle);
        $handle = fopen(LoanDao::FILE_CPT_LOAN, ConstanteDao::FILE_OPTION_W_PLUS);
        fputs($handle, $currentId+1);
        fclose($handle);
        return $currentId;
    }

    public function getById($motif): Loan
    {
        return $this->getOneByAttribute(LoanDao::CHAMP_ID, $motif);
    }

    public function getAll(): array
    {
        $handle = fopen(LoanDao::FILE_CPT_LOAN, ConstanteDao::FILE_OPTION_R);
        $entities = [];

        $entetes = fgetcsv($handle, 0, ConstanteDao::DELIM);

        while (($entity = fgetcsv($handle, 0, ConstanteDao::DELIM)) != false) {
            $entities[] = Loan::ClassFromArray(array_combine($entetes, $entity));
        }

        fclose($handle);
        return $entities;
    }

    public function getOneByAttribute(string $attribute, string $motif): Loan
    {
        $allEntities = $this->getAll();
        foreach ($allEntities as $entity) {
            $getter = "get".ucfirst($attribute);
            if (strtolower($entity->$getter()) === strtolower($motif)) {
                return $entity;
            }
        }
        return null;
    }
}