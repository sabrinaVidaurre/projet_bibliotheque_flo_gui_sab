<?php

require_once "ConstanteDao.php";
require_once "C:\EnvDev\projets_git\projet_bibliotheque_flo_gui_sab\Bibliotheque\services\dto\books.php";

class BookDao
{
    private const FILE_SAVE_BOOK = "C:\wamp64\www\Bibliotheque\data\save_book.csv";
    private const FILE_CPT_BOOK = "C:\wamp64\www\Bibliotheque\data\cpt_book.txt";
    private const CHAMP_ID = "id";
    private const CHAMP_TITRE = "titre";
    private const CHAMP_NB_LIVRE = "nbLivre";
    private const ENTETES_BOOKS = 
    [BookDao::CHAMP_ID,
    BookDao::CHAMP_TITRE,
    BookDao::CHAMP_NB_LIVRE];

    public function save(Book $newBooks): Book
    {
        $handle = fopen(BookDao::FILE_SAVE_BOOK, ConstanteDao::FILE_OPTION_A_PLUS);
        $newBooks->setId(str_pad($this->getNextId(), 3, "0", STR_PAD_LEFT));
        fputcsv($handle, $newBooks->toArray(), ConstanteDao::DELIM);
        fclose($handle);
        return $newBooks;
    }

    public function getNextId(): int
    {
        $handle = fopen(BookDao::FILE_CPT_BOOK, ConstanteDao::FILE_OPTION_A_PLUS);
        $currentId = intval(fgets($handle));
        fclose($handle);
        $handle = fopen(BookDao::FILE_CPT_BOOK, ConstanteDao::FILE_OPTION_W_PLUS);
        fputs($handle, $currentId+1);
        fclose($handle);
        return $currentId;
    }
    public function getById($motif): Book
    {
        return $this->getOneByAttribute(BookDao::CHAMP_ID, $motif);
    }

    public function getAll(): array
    {
        $handle = fopen(BookDao::FILE_CPT_BOOK, ConstanteDao::FILE_OPTION_R);
        $entities = [];

        $entetes = fgetcsv($handle, 0, ConstanteDao::DELIM);

        while (($entity = fgetcsv($handle, 0, ConstanteDao::DELIM)) != false) {
            $entities[] = Book::ClassFromArray(array_combine($entetes, $entity));
        }

        fclose($handle);
        return $entities;
    }

    public function getOneByAttribute(string $attribute, string $motif): Book
    {
        $allEntities = $this->getAll();
        foreach ($allEntities as $entity) {
            $getter = "get".ucfirst($attribute);
            if (strtolower($entity->$getter()) === strtolower($motif)) {
                return $entity;
            }
        }
        return null;
    }
}