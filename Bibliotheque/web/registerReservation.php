<?php
session_start();
// if (isset($_SESSION['idprof'])) {
// 	header('location:accueil.php') ;	
// }

?>
<!DOCTYPE html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style1.css">
  <title> Ajouter un livre </title>
</head>

<body>

  <br>
  <br>

  <section>
    <h1>Enregistrer une réservation</h1>
    <br>

    <div id="form">
      <form method="POST" action="doAddBook.php">
        <label>Date de réservation : </label><input required type="date" name="date" placeholder="Date début d'emprunt">
        <label>Id de l'adhérent :</label> <input required type="number" name="idSub" placeholder="ID Adhérent">
        <label>Id du livre :</label> <input required type="number" name="idBook" placeholder="ID Livre">
        <div id="button"><button>Envoyer</button></div>
      </form>
    </div>
  </section>

</body>

</html>