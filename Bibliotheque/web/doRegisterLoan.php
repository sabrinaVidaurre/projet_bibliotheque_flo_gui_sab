<?php 

require_once "C:\wamp64\www\Bibliotheque\services\dto\Subscriber.php";
require_once "C:\wamp64\www\Bibliotheque\services\dao\SubscriberDao.php";
require_once "C:\wamp64\www\Bibliotheque\services\dto\Book.php";
require_once "C:\wamp64\www\Bibliotheque\services\dao\BookDao.php";
require_once "C:\wamp64\www\Bibliotheque\services\dto\Loan.php";
require_once "C:\wamp64\www\Bibliotheque\services\dao\LoanDao.php";

$tmpLoan = new Loan();

$tmpDate1 = new DateTime($_POST["dateDebut"]);
$tmpDate2 = new DateTime($_POST["dateFin"]);

$tmpLoan->setDateDebut($tmpDate1);
$tmpLoan->setDateFin($tmpDate2);

$tmpBook = new BookDao();
$tmpSub = new SubscriberDao();

$tmpBook = $tmpBook->getById($_POST["idBook"]);
$tmpSub = $tmpSub->getById($_POST["idSub"]);

$tmpLoan->setBookId($tmpBook);
$tmpLoan->setSubscriberId($tmpSub);

$tmpLoanDAO = new LoanDao();
$tmpLoanDAO->save($tmpLoan); 


?>