<?php
session_start();
// if (isset($_SESSION['idprof'])) {
// 	header('location:accueil.php') ;	
// }

?>
<!DOCTYPE html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style1.css">
  <title> Ajouter un livre </title>
</head>


<body>

  <br>

  <section>

    <h1>Enregistrer un emprunt</h1>

    <br>

    <div id="form">
      <form method="POST" action="doRegisterLoan.php">
        <label>Date début d'emprunt :</label><input required type="date" name="dateDebut" placeholder="Date début d'emprunt">
        <label>Date Fin d'emprunt : </label><input required type="date" name="dateFin" placeholder="Date fin d'emprunt">
        <label>Id de l'adhérent : </label><input required type="number" name="idSub" placeholder="ID Adhérent">
        <label>Id du livre :</label> <input required type="number" name="idBook" placeholder="ID Livre">
        <div id="button"><button>Envoyer</button></div>
      </form>
    </div>
  </section>

</body>

</html>