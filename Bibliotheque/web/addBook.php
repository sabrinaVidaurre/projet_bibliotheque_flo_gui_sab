<?php
session_start();
// if (isset($_SESSION['idprof'])) {
// 	header('location:accueil.php') ;	
// }

?>
<!DOCTYPE html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style1.css">
  <title> Ajouter un livre </title>
</head>

<body>

  <section>
    <h1>Ajouter un livre dans la bibliothèque</h1>
    <br>

    <div id="form">
      <form method="POST" action="doAddBooks.php">
        <label>Titre :</label> <input required type="text" name="title" placeholder="Titre du livre">
        <label>Nombre de livres :</label> <input required type="number" name="nbLivres" placeholder="Nombre de livres">
        <div id="button"><button>Envoyer</button></div>
      </form>
    </div>
  </section>

</body>

</html>