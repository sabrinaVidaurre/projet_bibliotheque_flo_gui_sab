<?php
session_start();
$errorMsg = "";
$validUser = $_SESSION["login"] === true;
if (isset($_POST["sub"])) {
    $validUser = (($_POST["username"] == "admin") && ($_POST["password"] == "password"));
    if (!$validUser) $errorMsg = "Invalid username or password." . PHP_EOL;
    else $_SESSION["login"] = true;
}
if ($validUser) {
    header("Location: /quelquePart.php");
    //dans quelquePart.php -> session_start() + test $_SESSION['login'] === true ;
    die();
}
